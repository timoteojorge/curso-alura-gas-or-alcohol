package gasoralcohol.timoteojorge.com.br.gasoralcohol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText alcoholPrice;
    private EditText gasPrice;
    private Button checkButton;
    private TextView resultText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alcoholPrice = (EditText) findViewById(R.id.text_alcohol_price);
        gasPrice = (EditText) findViewById(R.id.text_gas_id);
        checkButton = (Button) findViewById(R.id.button_check_price_id);
        resultText = (TextView) findViewById(R.id.text_result_id);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String alcoholPriceText = alcoholPrice.getText().toString();
                String gasPriceText = gasPrice.getText().toString();

                Double alcoholPriceDouble = Double.parseDouble(alcoholPriceText);
                Double gasPriceDouble = Double.parseDouble(gasPriceText);

                double result = alcoholPriceDouble/gasPriceDouble;

                if(result >= 0.7){
                    resultText.setText("It's better to use gas");
                    return;
                }
                resultText.setText("It's better to use alcohol");


            }
        });
    }
}
